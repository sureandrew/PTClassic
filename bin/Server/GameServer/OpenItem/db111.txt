//		아이템 정보		//


*이름		"홀리 부츠"
*Name		"Holy Boots"
*코드		"DB111"

///////////////	공통사항		 ////////////

*내구력		65 85
*무게		21
*가격		46000

///////////////	원소		/////////////

*생체		2 4
*불		2 4
*냉기		2 4
*번개		2 4
*독		2 4

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		
*사정거리		
*공격속도		
*명중력		
*크리티컬		

//////////////	방어성능		/////////////
// 추가요인

*흡수력		0.9 1.599
*방어력		50 75
*블럭율		

//////////////	이동성능		/////////////
// 추가요인

*이동속도	2.3 2.599

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인


*생명력재생	
*기력재생
*근력재생
*생명력추가
*기력추가		
*근력추가
*마법기술숙련도		

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		55
*힘		60
*정신력		56
*재능		
*민첩성		55
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승
*기력상승		
*근력상승

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능
**특화랜덤 Mechanician Fighter Pikeman Archer Knight Atalanta Priestess Magician
// Prayer Shaman //

**흡수력	0.5	0.799
**이동속도	0.4	0.899

*연결파일        "name\db111.zhoon"